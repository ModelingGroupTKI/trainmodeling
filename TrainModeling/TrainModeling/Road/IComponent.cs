﻿namespace TrainModeling
{
	public interface IComponent
	{
		int State { get; }
	}
}