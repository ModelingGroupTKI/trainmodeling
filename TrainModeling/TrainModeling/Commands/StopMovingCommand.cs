﻿using System;
using JetBrains.Annotations;

namespace TrainModeling.Commands
{
	public class StopMovingCommand : ICommand
	{
		private readonly IVehicle _vehicle;

		public StopMovingCommand([NotNull] IVehicle vehicle)
		{
			if (vehicle == null) throw new ArgumentNullException();
			_vehicle = vehicle;
		}

		public void Execute()
		{
			_vehicle.StopMoving();
		}
	}
}