﻿namespace TrainModeling.Commands
{
	public interface ICommand
	{
		void Execute();
	}
}